<?php

namespace App\Policies;

use App\User;
use App\recetas;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecetasPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\recetas  $recetas
     * @return mixed
     */
    public function view(User $user, recetas $recetas)
    {
        return $user->id === $recetas->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\recetas  $recetas
     * @return mixed
     */
    public function update(User $user, recetas $recetas)
    {
        // Revisar si el usuario autenticado es el mismo que crea la receta 
        return $user->id === $recetas->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\recetas  $recetas
     * @return mixed
     */
    public function delete(User $user, recetas $recetas)
    {
        // Revisar si el usuario autenticado es el mismo que crea la receta
        return $user->id === $recetas->user_id; 
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\recetas  $recetas
     * @return mixed
     */
    public function restore(User $user, recetas $recetas)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\recetas  $recetas
     * @return mixed
     */
    public function forceDelete(User $user, recetas $recetas)
    {
        //
    }
}
