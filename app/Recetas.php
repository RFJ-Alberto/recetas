<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recetas extends Model
{
    // campos que se agregaran
    protected $fillable = [
        'titulo', 'preparacion' ,'ingredientes', 'imagen', 'categoria_id'
    ];


    //obtiene la categoria de la receta via FK
    public function categoria()
    {
        return $this->belongsTo(CategoriaReceta::class);
    }

    // obtiene la información de el usuario via FK
    public function autor()
    {
        return $this->belongsTo(User::class, 'user_id'); // el segundo parametro 'user_id' es para indicar en que columna de esa tabla se guarda la relacion.
    }
}
