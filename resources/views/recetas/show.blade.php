@extends('layouts.app')

@section('content')
{{-- <h1> {{ $recetas }} </h1> --}}
    <article class="contenido-receta">
        <h1 class="text-center mb-4"> {{ $recetas->titulo }}  </h1>

        <div class="imagen-receta">
            <img src="/storage/{{ $recetas->imagen }}" class="w-100" alt="">
        </div>

        <div class="receta-meta mt-2">
            <p>
                <span class="font-weight-bold text-primary"> Escrito en: </span>
                {{ $recetas->categoria->nombre }}
            </p>
            
            <p>
                <span class="font-weight-bold text-primary"> Autor: </span>
                {{ $recetas->autor->name }}
            </p>

            <p>
                <span class="font-weight-bold text-primary"> Fecha: </span>
                @php
                    $fecha = $recetas->created_at     
                @endphp

                {{-- <fecha-receta> es el componente que creamos con vue 'resources>js>components>FechaReceta.vue'  --}}
                <fecha-receta fecha = "{{ $fecha }}" >  </fecha-receta>
            </p>


            <div class="ingredientes">
                <h2 class="my-3 text-primary"> Ingredientes </h2>
                {!! $recetas->ingredientes !!}
            </div>
      
            <div class="preparacion">
                <h2 class="my-3 text-primary"> Preparación </h2>
                {!! $recetas->preparacion !!}
            </div>
        </div>

    </article>

@endsection