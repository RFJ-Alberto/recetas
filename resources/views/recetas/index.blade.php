@extends('layouts.app')

@section('botones')
    @include('ui.navegacion')
@endsection


{{-- Este @section ('content') se insetará en el yield de /layouts/app.blade.php ya que extendiende de el layouts--}}
@section('content')

    <h2 class="text-center mb-5"> Administra tus recetas </h2>

    <div class="col-md-10 mx-auto bg-white p-3">
        <table class="table">
            <thead class="bg-primary text-light">
                <tr>
                    <th scole="col"> Título </th>
                    <th scole="col"> Categoría </th>
                    <th scole="col"> Acciones </th>
                </tr>
            </thead>

            <tbody>
                {{-- Se leen las recetas creadas por el usuario y se muestran en una tabla --}}
                @foreach ($recetas as $receta)
                    <tr>
                        <td> {{ $receta->titulo }} </td>
                        <td> {{ $receta->categoria->nombre }} </td>
                        <td>
                            {{-- Eliminar de forma sencillita --}}
                            {{-- <form action="{{ route('recetas.destroy', ['recetas' => $receta->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger w-100 mb-2 d-block" value="Eliminar">
                            </form> --}}

                            {{-- Eliminar mediante componente de vuejs --}}
                            <eliminar-receta receta-id={{ $receta->id }}></eliminar-receta>

                            <a href="{{ route('recetas.edit', ['recetas' => $receta->id]) }}" class="btn btn-dark mb-2 d-block"> Editar </a>
                            <a href="{{ route('recetas.show', ['recetas' => $receta->id]) }}" class="btn btn-success mb-2 d-block"> Ver </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="col-12 mt-4 justify-content-center d-flex">
            {{ $recetas->links() }}
        </div>

    </div>

@endsection


